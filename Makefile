# GNU Make
#

PYTHON        := python3
CSVNAME       := imageFeatures.csv
mod_dir       := EMClassifier
val_dir       := metrics
bkp_dir       := backups
trained_model := trainedMLP.joblib
val_sources   := crossvalidation.py plot_metrics.py
val_csv       := crossvalidation.csv
bkp_name       = BACKUP$(shell date --iso=seconds)

CP := -cp -r
MV := mv

all:

.PHONY: features
features: PYFLAGS = -d1 $(DIR1) -d2 $(DIR2)
features: feature_extraction.py
	$(CP) $(CSVNAME) $(bkp_dir)/$(CSVNAME)$(bkp_name)
	$(PYTHON) $< $(PYFLAGS)

.PHONY: train
train: training.py
	$(CP) $(addprefix $(mod_dir)/,$(trained_model)) $(addprefix $(bkp_dir)/,$(trained_model)$(bkp_name))
	$(PYTHON) $<
	$(MV) $(trained_model) $(mod_dir)

.PHONY: validate
validate: $(addprefix $(val_dir)/,$(val_sources))
	@$(foreach s,$^,$(PYTHON) $(s);)
	$(MV) $(val_csv) $(val_dir)/$(val_csv)
