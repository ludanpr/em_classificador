Dentro da pasta 'classificador':

## Extrair Características

    make features DIR1=<diretorio com imagens boas> DIR2=<diretorio com imagens ruins>
    
Para fazer um treinamento novo do zero, escolha sim para a pergunta
sobre sobrescrever o arquivo imageFeatures.csv

## Treinamento

    make train
    
## Validação

    make validate
