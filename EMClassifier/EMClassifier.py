"""
Esse arquivo implementa o classificador para imagens.

"""
import cv2
import pandas as pd
import numpy as np
from concurrent.futures import ProcessPoolExecutor
import joblib
from EMClassifier import features
####################################
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn
####################################
import sklearn.preprocessing
import sklearn.pipeline
from sklearn.neural_network import MLPClassifier

import os

class EMClassifier:
    def __init__(self):
        self.__datasetname = "imageFeatures.csv"
        self.__trainedname = "trainedMLP.joblib"

    def __extract_features(self, img=None):
        """! \brief Função de baixo nível (Para uso exclusivo da implementação).

        \param img  imagem cujas características serão extraídas
        """
        try:
            dark = features.verific_dark(img)
        except ZeroDivisionError:
            dark = 0.0

        blur     = features.detect_blur_gray(img)
        contrast = features.contrast(img)
        return dark, blur, contrast

    def __get_dataset(self, filename):
        ds = pd.read_csv(filename, header=None)

        classes  = ds.iloc[:, 0]
        features = ds.iloc[:, 1:]
        return classes, features

    def getImageFeatures(self, img):
        """! \brief  Obtém o vetor de características da imagem.

        \param img   a image a ser testada
        """
        if len(img.shape) != 3:
            raise TypeError("'img' deve estar em formato BGR")

        features = np.array(list(self.__extract_features(img)))
        return features

    def train(self):
        try:
            classes, features = self.__get_dataset(self.__datasetname)
        except FileNotFoundError as err:
            print("[ERRO FATAL]: {}".format(err))
            return None

        # Modifique os hiperparâmetros aqui, caso necessário
        clf = sklearn.pipeline.Pipeline([('scaler', sklearn.preprocessing.StandardScaler()),
                                         ('mlp', MLPClassifier(activation='relu', solver='lbfgs',
                                                               alpha=1e-04, learning_rate='invscaling',
                                                               max_iter=100, random_state=0))])
        clf.fit(features, classes)
        joblib.dump(clf, self.__trainedname)

    def EMMLPClassifier(self, img):
        """! \brief   Função que classifica a imagem 'img' em boa ou ruim usando
                      Multi-layer Perceptron.

        O argumento recebido em 'img' pode ser uma única imagem  ou pode ser uma
        lista contendo várias imagens que serão processadas.\n Se um erro fatal não
        ocorrer, retorna as classes das imagens classificadas e suas respectivas
        probabilidades (em dois arrays diferentes).\n\n
        Por exemplo, se 'img' for uma imagem:\n
            <b>([0], [1.0])     uma imagem boa com probabilidade 1.0\n
        Se 'img' é uma lista com 3 imagens:\n
            ([0, 1, 1], [.50, .75, 1.0])  a primeira imagem da lista boa,
                                          a segunda e terceira  ruins  e suas
                                          respectivas probabilidades.</b>

		\param img    imagem a ser classificada ou uma lista de imagens.
		\retval [1]   se a imagem for classificada como ruim
		\retval [0]   do contrário
		\retval None  se um erro fatal ocorrer

        \retval [prob] a probabilidade da classe prevista
        """
        assert type(img) is np.ndarray or type(img) is list,\
            'imgheader não é numpy.ndarray nem uma lista de numpy.ndarray'

        try:
            clf = joblib.load(os.path.join(os.path.dirname(__file__), self.__trainedname))
        except FileNotFoundError as err:
            print("[ERRO FATAL]: {}".format(err))
            return None

        try:
            if type(img) is list:
                with ProcessPoolExecutor() as executor:
                    results      = executor.map(self.getImageFeatures, img)
                    testfeatures = np.array(list(results))
            else:
                testfeatures = np.array([self.getImageFeatures(img)])
        except TypeError as err:
            print("[ERRO FATAL]: {}".format(err))
            return None

        pred = clf.predict(testfeatures)
        i    = np.arange(testfeatures.shape[0])
        return pred, clf.predict_proba(testfeatures)[i, pred]
