"""
Características usadas pelo classificador.
"""
import numpy as np
#import skimage.feature
import imutils
import cv2

def verific_dark(image_to_analyse: np.ndarray, *, count_percent=0.09) -> float:
    img = imutils.resize(image_to_analyse, width=64)
    lab= cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    y, x, z = img.shape
    l_blur = cv2.GaussianBlur(l, (11, 11), 5)

    row_percent    = int(count_percent*x) # 1% do total de pixels (linha)
    column_percent = int(count_percent*y) # 1% do total de pixels (coluna)

    maxval = []
    for i in range(1,x-1):
        if i % row_percent == 0:
            for j in range(1, y-1):
                if j % column_percent == 0:
                    img_segment = l_blur[i:i+3, j:j+3]
                    _, maxVal, _, _ = cv2.minMaxLoc(img_segment)
                    maxval.append(maxVal)

    return round(sum(maxval) / len(maxval))

def detect_blur_gray(img: np.ndarray) -> float:
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return cv2.Laplacian(gray, cv2.CV_64F).var()

def contrast(img: np.ndarray) -> float:
    g = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return g.std()

# def detect_lbp(image: np.ndarray, num_point: int = 24, radius: int = 8, eps:float = 1e-7):
#     gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

#     lbp = skimage.feature.local_binary_pattern(gray, num_point, radius, method="uniform")
#     (hist, _) = np.histogram(lbp.ravel(), bins=np.arange(0, num_point+3), range= (0, num_point +2))
#     hist = hist.astype("float")
#     hist /= (hist.sum() + eps)
#     return np.mean(hist), np.var(hist)
