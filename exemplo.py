from EMClassifier import EMClassifier
from cv2 import imread
import sys

if __name__ == '__main__':
    # USO: python3 exemplo.py <caminho da imagem>
    img = imread(sys.argv[1])

    cl = EMClassifier.EMClassifier()
    label, prob = cl.EMMLPClassifier(img)

    print("Classe: {}, Probabilidade = {}".format(label, prob))
