""" Arquivo .py:
				feature_extraction.py

    Script que cria o dataset de características (imageFeatures.csv)

"""
from cv2 import imread
import os
import concurrent.futures
import argparse
import csv
from EMClassifier import EMClassifier

def ret_features(noise, imgpath=None):
    cl = EMClassifier.EMClassifier()
    feats = [noise]
    feats.extend(cl._EMClassifier__extract_features(imread(imgpath)))
    return feats

def features_list(dirhq, dirns):
    images = []
    for root, _, files in os.walk(dirhq, topdown=True):
        for f in files:
            imgpath = os.path.join(root, f)
            images.append((0, imgpath))
    for root, _, files in os.walk(dirns, topdown=True):
        for f in files:
            imgpath = os.path.join(root, f)
            images.append((1, imgpath))

    features = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        results = [executor.submit(ret_features, noise, imgpath) for noise, imgpath in images]
        for r in concurrent.futures.as_completed(results):
            features.append(r.result())

    return features

def init_csv_file(dirhq, dirns):
    mode = 'a'
    ans  = 'n'
    
    filename = "imageFeatures.csv"
    if os.path.exists(os.path.join(os.getcwd(), filename)):
        ans = input("{} já existe. Deseja sobrescrevê-lo? [S/n]".format(filename))
    if ans == 's' or ans == 'S':
        mode = 'w'

    features = features_list(dirhq, dirns)
    with open(filename, mode, newline='') as f:
        writer = csv.writer(f)
        writer.writerows(features)


if __name__ == '__main__':
    # USO: python3 feature_extraction.py -d1 <diretório-imagens-boas> -d2 <diretório-imagens-ruins>
    parser = argparse.ArgumentParser()
    obrigatorio = parser.add_argument_group("argumentos obrigatorios")
    obrigatorio.add_argument("-d1", "--hqdirpath", help="caminho do diretório com imagens boas", required=True)
    obrigatorio.add_argument("-d2", "--noisedirpath", help="caminho do diretória com imagens ruins", required=True)

    args = parser.parse_args()
    init_csv_file(args.hqdirpath, args.noisedirpath)
