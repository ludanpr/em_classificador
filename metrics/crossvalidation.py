import pandas as pd
import numpy as np
import sklearn.metrics
import sklearn.model_selection
import sklearn.pipeline
import sklearn.preprocessing
import matplotlib.pyplot as plt
import joblib
import csv

if __name__ == '__main__':
    im_feats = pd.read_csv("imageFeatures.csv", header=None)

    y = im_feats.iloc[:, 0]
    X = im_feats.iloc[:, 1:]

    clf = joblib.load("EMClassifier/trainedMLP.joblib")

    scores = [
        sklearn.model_selection.cross_val_score(clf, X, y, cv=5, scoring='accuracy'),
        sklearn.model_selection.cross_val_score(clf, X, y, cv=5, scoring='precision'),
        sklearn.model_selection.cross_val_score(clf, X, y, cv=5, scoring='recall'),
        sklearn.model_selection.cross_val_score(clf, X, y, cv=5, scoring='f1'),
    ]
    metrics = [ 'accuracy', 'precision', 'recall', 'f1' ]

    with open("crossvalidation.csv", 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(scores)
        for m, s in zip(metrics, scores):
            writer.writerow([m, s.mean()])
