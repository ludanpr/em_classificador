import sklearn
import seaborn
import pandas as pd
import joblib
import matplotlib.pyplot as plt
import sklearn.model_selection
import sklearn.preprocessing
import sklearn.pipeline
from sklearn.neural_network import MLPClassifier

if __name__ == '__main__':
    im_feats = pd.read_csv("imageFeatures.csv", header=None)

    y = im_feats.iloc[:, 0]
    X = im_feats.iloc[:, 1:]
    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=.3, random_state=0)
    clf = sklearn.pipeline.Pipeline([('scaler', sklearn.preprocessing.StandardScaler()),
                                     ('mlp', MLPClassifier(activation='relu', solver='lbfgs',
                                                           alpha=1e-04, learning_rate='invscaling',
                                                           max_iter=100, random_state=0))])
    clf.fit(X_train, y_train)

    sklearn.metrics.plot_roc_curve(clf, X_test, y_test)
    sklearn.metrics.plot_confusion_matrix(clf, X_test, y_test)
    plt.show()
